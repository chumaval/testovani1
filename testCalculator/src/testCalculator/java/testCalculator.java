import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class testCalculator {
    @Test
    public void CalculatorTest(){
        Calculator calc = new Calculator();
        assertEquals(calc.addition(1, 8), 9);
        assertEquals(calc.subtraction(10, 5), 5);
        assertEquals(calc.multiplication(3, 5), 15);
        assertEquals(calc.division(15, 3), 5);
    }
    @Test
    public void CalculatorError(){
        Calculator error = new Calculator();
        assertEquals(error.division(15, 0), 3);
    }
}
