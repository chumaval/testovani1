public class Factorial {

    public long Factorial(long cislo){
        if (cislo == 1){
            return 1;
        }else if (cislo < 1){
            return -1;
        } else {
            return cislo*Factorial(cislo -1 );
        }
    }
}
